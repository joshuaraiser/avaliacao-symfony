<?php

namespace AppBundle\Entity;

/**
 * Sale
 */
class Sale
{
    /**
     * @var \DateTime
     */
    private $sale_date;

    /**
     * @var string
     */
    private $sale_total_price;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Client
     */
    private $client;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $items;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set saleDate
     *
     * @param \DateTime $saleDate
     *
     * @return Sale
     */
    public function setSaleDate($saleDate)
    {
        $this->sale_date = $saleDate;

        return $this;
    }

    /**
     * Get saleDate
     *
     * @return \DateTime
     */
    public function getSaleDate()
    {
        return $this->sale_date;
    }

    /**
     * Set saleTotalPrice
     *
     * @param string $saleTotalPrice
     *
     * @return Sale
     */
    public function setSaleTotalPrice($saleTotalPrice)
    {
        $this->sale_total_price = $saleTotalPrice;

        return $this;
    }

    /**
     * Get saleTotalPrice
     *
     * @return string
     */
    public function getSaleTotalPrice()
    {
        return $this->sale_total_price;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param \AppBundle\Entity\Client $client
     *
     * @return Sale
     */
    public function setClient(\AppBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \AppBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add item
     *
     * @param \AppBundle\Entity\SaleItem $item
     *
     * @return Sale
     */
    public function addItem(\AppBundle\Entity\SaleItem $item)
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \AppBundle\Entity\SaleItem $item
     */
    public function removeItem(\AppBundle\Entity\SaleItem $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }
}
