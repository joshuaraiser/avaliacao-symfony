<?php

namespace AppBundle\Entity;

/**
 * SaleItem
 */
class SaleItem
{
    /**
     * @var string
     */
    private $price;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Product
     */
    private $product;


    /**
     * Set price
     *
     * @param string $price
     *
     * @return SaleItem
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return SaleItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \AppBundle\Entity\Product $product
     *
     * @return SaleItem
     */
    public function setProduct(\AppBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Generates the magic method
     *
     */
    public function __toString(){
        // to show the name of the Client in the select
        return $this->product->getName();
        // to show the id of the Client in the select
        // return $this->id;
    }

    /**
     * @var \AppBundle\Entity\Sale
     */
    private $sale;


    /**
     * Set sale
     *
     * @param \AppBundle\Entity\Sale $sale
     *
     * @return SaleItem
     */
    public function setSale(\AppBundle\Entity\Sale $sale = null)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return \AppBundle\Entity\Sale
     */
    public function getSale()
    {
        return $this->sale;
    }
}
