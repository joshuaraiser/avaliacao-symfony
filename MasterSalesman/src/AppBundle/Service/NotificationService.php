<?php
/**
 * Created by PhpStorm.
 * User: Joshua Raiser
 * Date: 05/02/2019
 * Time: 20:02
 */

namespace AppBundle\Service;


use Doctrine\ORM\EntityManagerInterface;

class NotificationService
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $manager, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->entityManager = $manager;
        $this->twig = $twig;
    }

    public function notifyLowStock()
    {
        $productLowStock = $this->getAllLowStockProducts();
        $productSupervisors = $this->getAllProductSupervisors();

        if(!empty($productLowStock))
        {
            foreach($productSupervisors as $supervisor)
            {
                $body = $this->twig->render('Email/Notify/low_stock_products.html.twig',
                    ['products' => $productLowStock]
                );

                $message = (new \Swift_Message('Hello Email'))
                    ->setSubject('MasterSalesman - Product\'s quantity low in stock')
                    ->setFrom('no-reply@mastersalesman.com')
                    ->setTo($supervisor->getEmail())
                    ->setBody($body, 'text/html');

                $this->mailer->send($message);
            }
        }


    }

    private function getAllLowStockProducts()
    {
        $productRepository = $this->entityManager->getRepository('AppBundle:Product');
        return $productRepository->findAllProductsWithFiveOrLessStockQuantity();
    }

    private function getAllProductSupervisors()
    {
        $users = $this->entityManager->getRepository('AppBundle:User')->findAll();

        $productSupervisors = [];

        foreach($users as $user)
        {
            foreach($user->getRoles() as $role)
            {
                if($role == 'ROLE_ADMIN' || $role == 'ROLE_PRODUCT_MANAGER')
                {
                    $productSupervisors[] = $user;
                }
            }
        }

        return $productSupervisors;

    }


}