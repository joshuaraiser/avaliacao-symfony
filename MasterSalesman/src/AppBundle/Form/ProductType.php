<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'empty_data' => 'Product without name',
                'label' => 'Product name'
            ])
            ->add('price', MoneyType::class, [
                'required' => true,
                'label' => 'Price US$',
                'currency' => 'US'
            ])
            ->add('stock_quantity', NumberType::class, [
                'required' => true
            ])
            ->add('categories', EntityType::class, [
                'required' => true,
                'multiple' => true,
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'empty_data' => 'There are no registered categories!',
                'label' => 'Product category'
            ]);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_product';
    }


}
