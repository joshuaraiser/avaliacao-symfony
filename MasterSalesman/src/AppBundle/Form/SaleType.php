<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class SaleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sale_date', DateType::class, [
                'disabled' => true
            ])
            ->add('sale_total_price', MoneyType::class, [
                'label' => 'Total price US$',
                'currency' => 'US',
                'disabled' => true
            ])
            ->add('client', EntityType::class, [
                'class' => 'AppBundle:Client',
                'required' => true,
                'label' => 'Client name',
                'empty_data' => 'There are no registered customers!',
                'choice_label' => 'name'
            ])
            ->add('items', CollectionType::class, [
                'required' => true,
                'entry_type' => SaleItemType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'label' => false
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Sale'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_sale';
    }


}
