<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Client;
use Doctrine\DBAL\DBALException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Client controller.
 *
 */
class ClientController extends Controller
{

    /**
     * Lists all client entities.
     * @IsGranted("ROLE_CLIENT_MANAGER")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clients = $em->getRepository('AppBundle:Client')->findAll();

        return $this->render('client/index.html.twig', array(
            'clients' => $clients,
        ));
    }

    /**
     * Creates a new client entity.
     * @IsGranted("ROLE_CLIENT_MANAGER")
     */
    public function newAction(Request $request)
    {
        $client = new Client();
        $form = $this->createForm('AppBundle\Form\ClientType', $client);
        $form->handleRequest($request);

        $documentValid = $this->documentIsValid($client->getDocumentNumber());

        $clientExist = $this->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Client')
            ->findBy(['document_number' => $client->getDocumentNumber()]);


        if ($form->isSubmitted() && $form->isValid() && $documentValid) {

            try
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($client);
                $em->flush();

                return $this->redirectToRoute('client_show', array('id' => $client->getId()));
            }
            catch (DBALException $ex)
            {
                $this->addFlash('danger', 'The document number already exist!');
            }

        }

        if(!$documentValid)
        {
            $this->addFlash('warning', 'The document number must be valid!');
        }
        if(is_null($client->getId()) && $documentValid)
        {
            $this->addFlash('danger', 'The document number already exist!');
        }

        return $this->render('client/new.html.twig', array(
            'client' => $client,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a client entity.
     * @IsGranted("ROLE_CLIENT_MANAGER")
     */
    public function showAction(Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);

        return $this->render('client/show.html.twig', array(
            'client' => $client,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing client entity.
     * @IsGranted("ROLE_CLIENT_MANAGER")
     */
    public function editAction(Request $request, Client $client)
    {
        $deleteForm = $this->createDeleteForm($client);
        $editForm = $this->createForm('AppBundle\Form\ClientType', $client);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('client_show', array('id' => $client->getId()));
        }

        return $this->render('client/edit.html.twig', array(
            'client' => $client,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a client entity.
     * @IsGranted("ROLE_CLIENT_MANAGER")
     */
    public function deleteAction(Request $request, Client $client)
    {
        $form = $this->createDeleteForm($client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($client);
            $em->flush();
        }

        return $this->redirectToRoute('client_index');
    }

    /**
     * @IsGranted("ROLE_CLIENT_MANAGER")
     *
     * Creates a form to delete a client entity.
     *
     * @param Client $client The client entity
     *
     * @return \Symfony\Component\Form\Form The form
     *
     */
    private function createDeleteForm(Client $client)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('client_delete', array('id' => $client->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function documentIsValid($cpf) {

        // get numbers only
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // verify if the field has 11 numbers
        if (strlen($cpf) != 11) {
            return false;
        }
        // verify if its a sequence like 11111111111, 22222222222 ...
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // do the maths to validate the document
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

}
