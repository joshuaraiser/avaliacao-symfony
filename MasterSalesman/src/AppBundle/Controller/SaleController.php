<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Sale;
use AppBundle\Entity\SaleItem;
use AppBundle\Service\NotificationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Sale controller.
 *
 */
class SaleController extends Controller
{

    /**
     * @var NotificationService
     */
    private $notify;

    public function __construct(NotificationService $notificationService)
    {
        $this->notify = $notificationService;
    }

    /**
     * Lists all sale entities.
     * @IsGranted("ROLE_SALE_MANAGER")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sales = $em->getRepository('AppBundle:Sale')->findAll();

        return $this->render('sale/index.html.twig', array(
            'sales' => $sales,
        ));
    }

    /**
     * Creates a new sale entity.
     * @IsGranted("ROLE_SALE_MANAGER")
     */
    public function newAction(Request $request)
    {
        $sale = new Sale();
        $sale->setSaleDate(new \DateTime());
        $form = $this->createForm('AppBundle\Form\SaleType', $sale);
        $form->handleRequest($request);

        if (($form->isSubmitted() && $form->isValid()) || !is_null($request->request->get('sale_items_list'))) {

            $em = $this->getDoctrine()->getManager();

            $sale_information = $request->request->get('sale_items_list');

            $items = $sale_information[0]['items'];
            $client_id = $sale_information[0]['client_id'];
            $client = $this->getDoctrine()->getRepository('AppBundle:Client')->find($client_id);

            $sale->setClient($client);

            $total_price = 0;

            foreach ($items[0] as $item)
            {
                for($i = 0; $i < sizeof($item); $i++)
                {
                    if($item[$i]['product_id'] != -1)
                    {
                        $sale_item = new SaleItem();

                        $product = $this->getDoctrine()
                            ->getRepository('AppBundle:Product')
                            ->find($item[$i]['product_id']);

                        if($product->getStockQuantity() >= $item[$i]['quantity'])
                        {
                            $product_quantity_price = $product->getPrice() * $item[$i]['quantity'];
                            $total_price += $product_quantity_price;

                            $sale_item->setPrice($product_quantity_price);
                            $sale_item->setProduct($product);
                            $sale_item->setQuantity($item[$i]['quantity']);

                            $product->setStockQuantity($product->getStockQuantity() -  $sale_item->getQuantity());

                            $em->persist($sale_item);
                            $em->persist($product);

                            $sale->addItem($sale_item);

                        }
                        else
                        {
                            // error message: quantity out of stock
                        }
                    }
                }
            }

            $sale->setSaleTotalPrice($total_price);
            $em->persist($sale);
            $em->flush();

            $this->notify->notifyLowStock();

            return new Response($this->generateUrl('sale_show', ['id' => $sale->getId()]));
        }


        return $this->render('sale/new.html.twig', array(
            'sale' => $sale,
            'form' => $form->createView()
        ));
    }

    /**
     * @IsGranted("ROLE_SALE_MANAGER")
     * @param Request $request
     * @return Response
     */
    public function totalPriceAction(Request $request)
    {
        if(is_null($request->get('sale_items_list')))
        {
            // error message: none item added to the items's list
        }

        $items_list = $request->get('sale_items_list');

        $total_price = 0;

        foreach ($items_list as $item)
        {
            for($i = 0; $i < sizeof($item); $i++)
            {
                if($item[$i]['product_id'] != -1)
                {
                    $product = $this->getDoctrine()
                        ->getRepository('AppBundle:Product')
                        ->find($item[$i]['product_id']);

                    if($product->getStockQuantity() >= $item[$i]['quantity'])
                    {
                        $total_price += $product->getPrice() * $item[$i]['quantity'];
                    }
                    else
                    {
                        // error message: quantity out of stock
                    }
                }

            }
        }

        return new Response($total_price);
    }

    /**
     * Finds and displays a sale entity.
     * @IsGranted("ROLE_SALE_MANAGER")
     */
    public function showAction(Sale $sale)
    {
        $deleteForm = $this->createDeleteForm($sale);

        return $this->render('sale/show.html.twig', array(
            'sale' => $sale,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sale entity.
     * @IsGranted("ROLE_SALE_MANAGER")
     */
    public function editAction(Request $request, Sale $sale)
    {
        $items = $sale->getItems();

        foreach ($items as $item)
        {
            $sale->removeItem($item);
        }

        $deleteForm = $this->createDeleteForm($sale);
        $editForm = $this->createForm('AppBundle\Form\SaleType', $sale);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sale_edit', array('id' => $sale->getId()));
        }

        return $this->render('sale/edit.html.twig', array(
            'sale' => $sale,
            'items' => $items,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sale entity.
     * @IsGranted("ROLE_SALE_MANAGER")
     */
    public function deleteAction(Request $request, Sale $sale)
    {

        $form = $this->createDeleteForm($sale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sale);
            $em->flush();
        }

        return $this->redirectToRoute('sale_index');
    }

    /**
     * @IsGranted("ROLE_SALE_MANAGER")
     *
     * Creates a form to delete a sale entity.
     *
     * @param Sale $sale The sale entity
     *
     * @return \Symfony\Component\Form\Form The form
     *
     */
    private function createDeleteForm(Sale $sale)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('sale_delete', array('id' => $sale->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
