<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SaleItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Saleitem controller.
 *
 */
class SaleItemController extends Controller
{
    /**
     * Lists all saleItem entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $saleItems = $em->getRepository('AppBundle:SaleItem')->findAll();

        return $this->render('saleitem/index.html.twig', array(
            'saleItems' => $saleItems,
        ));
    }

    /**
     * Creates a new saleItem entity.
     *
     */
    public function newAction(Request $request)
    {
        $saleItem = new Saleitem();
        $form = $this->createForm('AppBundle\Form\SaleItemType', $saleItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($saleItem);
            $em->flush();

            return $this->redirectToRoute('saleitem_show', array('id' => $saleItem->getId()));
        }

        return $this->render('saleitem/new.html.twig', array(
            'saleItem' => $saleItem,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a saleItem entity.
     *
     */
    public function showAction(SaleItem $saleItem)
    {
        $deleteForm = $this->createDeleteForm($saleItem);

        return $this->render('saleitem/show.html.twig', array(
            'saleItem' => $saleItem,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing saleItem entity.
     *
     */
    public function editAction(Request $request, SaleItem $saleItem)
    {
        $deleteForm = $this->createDeleteForm($saleItem);
        $editForm = $this->createForm('AppBundle\Form\SaleItemType', $saleItem);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('saleitem_edit', array('id' => $saleItem->getId()));
        }

        return $this->render('saleitem/edit.html.twig', array(
            'saleItem' => $saleItem,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a saleItem entity.
     *
     */
    public function deleteAction(Request $request, SaleItem $saleItem)
    {
        $form = $this->createDeleteForm($saleItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($saleItem);
            $em->flush();
        }

        return $this->redirectToRoute('saleitem_index');
    }

    /**
     * Creates a form to delete a saleItem entity.
     *
     * @param SaleItem $saleItem The saleItem entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SaleItem $saleItem)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('saleitem_delete', array('id' => $saleItem->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
